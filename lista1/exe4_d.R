# Title     : Exercício 4, letra D - Lista 1
# Objective : Operações básicas sobre documentos com RI
# Created by: robsonpessoa
# Created on: 26/04/2020

library(tm)
library(SnowballC)

source("./lista1/exe4.R")
source("./modelos/modprobabilistico.R")

query <- "felicidade paz saúde"

probabilistico <- sim(docs = docList,
                      docs_relevantes = list(doc1, doc2),
                      query = query)

probabilistico
