# Title     : Exercício 4, letra E - Lista 1
# Objective : Operações básicas sobre documentos com RI
# Created by: robsonpessoa
# Created on: 26/04/2020

library(tm)
library(SnowballC)

source("./lista1/exe4.R")
source("./modelos/modvetorial.R")

query <- "felicidade paz saúde"

vetorial <- sim(docs = docList,
                query = query)

vetorial
